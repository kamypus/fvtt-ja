Hooks.once("init", async () => {
    game.settings.register("fvtt-ja", "langFiles", {
        type: Array,
        default: [],
        scope: 'world',
        config: false
    });
    game.settings.register("fvtt-ja", 'systemLangFile', {
        name: "システム言語ファイル",
        hint: "ゲームシステムを独自ローカライズする言語ファイルを指定。",
        type: String,
        default: "",
        scope: 'world',
        config: true,
        filePicker: "file",
        onChange: () => {
            window.location.reload()
        }
    });
    game.settings.register("fvtt-ja", 'langPath', {
        name: "言語ファイルフォルダ",
        hint: "モジュールを独自ローカライズする言語ファイルを格納するフォルダを指定。",
        type: String,
        default: "",
        scope: 'world',
        config: true,
        filePicker: "folder",
        onChange: directory => {
            FvttJa.resetLangFiles(directory, true)
        }
    });

    let systemLangFile = game.settings.get("fvtt-ja", "systemLangFile");
    if (systemLangFile != "") {
        let lang = game.system.languages.filter(value => value.lang === game.i18n.lang);
        if (lang.keys.length !== 0) {
            game.system.languages.forEach(value => {
                if (value.lang === game.i18n.lang) {
                    FvttJa.log(`システム言語ファイル置換「${value.path}」⇒「${systemLangFile}」`);
                    value.path = systemLangFile;
                }
            })
        } else {
            let mod = game.modules.get("fvtt-ja");
            if (mod) {
                FvttJa.log(`システム用言語ファイル追加「${systemLangFile}」`);
                game.system.languages.add({
                    "lang": game.i18n.lang,
                    "name": "日本語",
                    "path": systemLangFile
                })
            }
        }
    }
    let langPath = game.settings.get("fvtt-ja", "langPath");
    if (langPath != "") {
        //言語ファイルフォルダから読み込み
        // 登録済みの設定から独自言語ファイルを登録
        let langFiles = game.settings.get("fvtt-ja", "langFiles");
        langFiles.forEach(fname => {
            if (fname.endsWith(".json")) {
                let mod_name = fname.slice(fname.lastIndexOf('/') + 1, fname.lastIndexOf("-ja.json") != -1 ? fname.lastIndexOf("-ja.json") : fname.lastIndexOf(".json"));
                let mod = game.modules.get(mod_name);
                if (mod) {
                    let ja = mod.languages.filter(lang => lang.lang == game.i18n.lang);
                    if (ja.size > 0) {
                        ja.forEach(lang => {
                            FvttJa.log(`言語ファイル置換「${lang.path}」⇒「${fname}」`);
                            lang.path = fname
                        })
                    } else {
                        FvttJa.log(`言語ファイル追加「${fname}」`);
                        mod.languages.add({
                            "lang": game.i18n.lang,
                            "name": "日本語",
                            "path": fname
                        })
                    }
                } else if (mod_name == game.system.id || (!game.system.id && mod_name == game.system.name)) {
                    let ja = game.system.languages.filter(lang => lang.lang == "ja");
                    if (ja) {
                        FvttJa.log(`システム言語ファイル置換「${ja.path}」⇒「${fname}」`);
                        game.system.languages.map(lang => lang.path = fname)
                    } else {
                        FvttJa.log(`システム言語ファイル追加「${fname}」`);
                        game.system.languages.push({ "lang": "ja", "name": "日本語", "path": `${fname}` })
                    }
                } else {
                    FvttJa.log(`言語ファイル「${fname}」に対応するモジュール「${mod_name}」が有りません`);
                }
            }
        });
    }

    // Check unsupported modules
    if (game.i18n.defaultModule !== "fvtt-ja") {
        window.alert("fvtt-ja：本モジュールを使用する場合\nFVTT本体の設定で、デフォルト言語に「日本語：Foundry VTT（MRyas私家版）」に設定してください。");
        game.shutDown();
        game.logOut();
        while (game.i18n.defaultModule !== "fvtt-ja") {
            game.logOut()
        }
    } else {
        game.modules.forEach(module => {
            if (module.id === "foundryVTTja") {
                while (module.id === "foundryVTTja") {
                    window.alert("fvtt-ja：You must uninstall foundryVTTja to use fvtt-ja.");
                    game.shutDown();
                    game.logOut();
                }
            }
        })
    }
});

Hooks.on("ready", async () => {
    // 登録フォルダに格納されたフォルダと登録済み言語ファイルを比較して更新
    let langPath = game.settings.get("fvtt-ja", "langPath");
    FvttJa.resetLangFiles(langPath, false)
});

class FvttJa {
    static async resetLangFiles(directory, reload = false) {
        if (directory != "") {
            let langFiles = game.settings.get("fvtt-ja", "langFiles");
            let ret = await FilePicker.browse("data", directory);

            if (JSON.stringify(langFiles.sort()) != JSON.stringify(ret.files.sort())) {
                await game.settings.set("fvtt-ja", "langFiles", ret.files);
                if (reload || confirm("言語ファイルが変更されています\nリロードしますか？")) {
                    window.location.reload()
                }
            }
        }
        if (reload) window.location.reload()
    }

    static log(message) {
        if (typeof message === "string") {
            console.log("fvtt-ja | " + message)
        } else {
            console.log("fvtt-ja ⇒");
            console.log(message)
        }
    }
}
